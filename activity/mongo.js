/*
1. Using Robo3T, in our hotel database, create a "rooms collection" and insert a single document with the following details:
	name - single
	accommodates - 2
	price - 1000
	description - "A simple room with all the basic necessities"
	rooms_available - 10
	isAvailable - false

2. Insert multiple rooms with the following details:
	name - double
	accommodates - 3
	price - 2000
	description - "A room fit for a small family going on a vacation"
	rooms_available - 5
	isAvailable - false

	name - queen
	accommodates - 4
	price - 4000
	description - "A room with a queen sized bed perfect for a simple getaway"
	rooms_available - 15
	isAvailable - false

3. Find rooms with a price greater than or equal to 2000

4. Find a room with a price greater than or equal to 2000 and with rooms_available greater than 10

5. Update the "queen" room to have rooms_available to 0

6. Update all rooms with rooms_available greater than 0 and the isAvailable equal to false and set the availability of these rooms to true

7. Delete all rooms with rooms_available equal to 0

8. Find a room with the name "double" and project only the following fields:

	name
	accommodates
	price
	description
*/

db.rooms.insertOne(
	{name: "single", accommodates: 2, price: 1000, description: "A simple room with all the basic necessities", rooms_Availabe: 10, isAvailable: false}
)

db.rooms.insertMany([
	{name: "double", accommodates: 3, price: 2000, description: "A room fit for a small family going on a vacation", rooms_Availabe: 5, isAvailable: false},
	{name: "queen", accommodates: 4, price: 4000, description: "A room with a queen sized bed perfect for a simple getaway", rooms_Availabe: 15, isAvailable: false}
])

db.rooms.find(
{
	price: {
		$gt: 2000}
}
)

db.rooms.find(
{
	price: {
		$gt: 2000
	},
	rooms_Availabe: {
		$gt: 10
	}
}
)

db.rooms.updateOne(
{
	 _id: ObjectId("62010651425e4a32920f1825")
},
{
	$set: {
		rooms_Availabe: 0
	}
}
)

db.rooms.updateMany(
{
	 rooms_Availabe: {
	 	$gt: 0},
	 	isAvailable: false
},
{
	$set: {
		isAvailable: true
	}
}
)

db.rooms.deleteOne(
	{
	_id: ObjectId("62010651425e4a32920f1825")
	}
)

db.rooms.find(
{
	name: "double"
},
{
	name: 1,
	accommodates: 1,
	price: 1,
	description: 1
}
)


// MongoDB Query Operators & Field Projection
/*
Query Operators - command line for mongodb
$set - is a query operator that replacces the value of a field with the specified value. If a given field does not yet exist in the record, $et will add a new field with the specified value.
-Update
db.users.updateOne(
	{
	_id: ObjectId("61fbcaa6d5b48a79bc5f15c0")
	},
	{
	$set: {
		skills: []
		  }
	}
)
$push - adds value
-Add
db.users.updateOne(
	{
	_id: ObjectId("61fbcaa6d5b48a79bc5f15c0")
	},
	{
	$push: {
		skills: "html"
		  }
	}
)
$each - 
-adds another value on an existing one
db.users.updateOne(
	{
	_id: ObjectId("61fbcaa6d5b48a79bc5f15c0")
	},
	{
	$push: {
		skills: {
			$each: ["CSS", "JavaScript"]
				}
		  }
	}
)
$unset - 
-remove
db.users.updateOne(
	{
	_id: ObjectId("61fbcaa6d5b48a79bc5f15c0")
	},
	{
	$unset: {
		skills: ""
		  }
	}
)
$in: search for specified value only
-Search for users that know either Javascript or MERN
db.users.find({
	skills: {
		$in: ["Javascript", "MERN"]
			  }
})
$all - search for specified value that has all the value inside the query
-Search for user that know HTML and CSS 
db.users.find({
	skills: {
		$all: ["HTML", "CSS"]
			  }
})
*/

/*
Mini Activity
create a list of five users, all with a name, age, isActive field, and skills array.

db.users.insertMany([
		{name: "John Smith", age: 25, isActive: true, skills: ["HTML", "CSS", "Bootstrap"]},
		{name: "Joe Ray", age: 24, isActive: true, skills: ["HTML", "CSS", "Javascript"]},
		{name: "Annie Han", age: 22, isActive: false, skills: ["HTML", "MERN"]},
		{name: "May Cee", age: 18, isActive: true, skills: ["HTML", "PHP", "MySQL"]},
		{name: "Rhian Ramos", age: 26, isActive: false, skills: ["JavaScript, "Python"]}
	])
*/

/*
Show listings that are property type "condominium", has a minimum stay of 1 night, has 1 bedroom, and can accomodate 2 people
-Filter the output list
db.listingsAndReviews.find(
	{
		property_type: "Condominium",
		minimum_nights: "2",
		bedrooms: 1,
		accommodates: 2
	},
	{
        name: 1,
		property_type: 1,
		minimum_nights: 1,
		bedrooms: 1,
		accommodates: 1
		}
)

.limit() - use to limit the number of output list, it will sort it out by default through ID
db.listingsAndReviews.find(
	{
		property_type: "Condominium",
		minimum_nights: "2",
		bedrooms: 1,
		accommodates: 2
	},
	{
        name: 1,
		property_type: 1,
		minimum_nights: 1,
		bedrooms: 1,
		accommodates: 1
		}
)
.limit(5)

Show listings that have a price of less than 100, fee for additional head is less than 20, and are in the country Portugal.
-Note: Use double quotation on dot notation if you want to look into an object. 
db.listingsAndReviews.find(
	{
		price: {
			$lt: 100
			   },
		extra_people: {
			$lt: 20
			   		  },
		"address.country": "Portugal"

	},
	{
		name: 1,
		price: 1,
		extra_people: 1,
		"address.country": 1
	}
)
.limit(5)

If you want to have a less than and greater than in a query, put them in together an object
db.listingsAndReviews.find(
	{
		price: {
			$lt: 100,
			$gt: 50}
			   },
{
		name: 1,
		price: 1,
		extra_people: 1,
		"address.country": 1
	}
)
.limit(5)

Search for the first 5 listings by name in ascending order that have a price less than 100.
-Sorting in Ascending order, Going up (a-z) use value of 1, going down or descending use value of -1 (z-a)
db.listingsAndReviews.find(
	{
		price: {
			$lt: 100
			   }
	},
	{
		name: 1,
		price: 1,
	}
)
.limit(5)
.sort({
	name: 1
})

Increase the price of all listings by 10
$inc - to increment values
db.listingsAndReviews.updateMany(
	{
		price: 
			{$lt: 100}
	},
	{
		$inc: 
		{price: 10}
	}
)

Decrement the price of all listings by 10
$inc - to decrement, use negative values
db.listingsAndReviews.updateMany(
	{
		price: 
			{$lt: 100}
	},
	{
		$inc: 
		{price: 10}
	}
)
*/
/*
delete listings with any one of the following conditions:
review scores accuracy <=75
review scores cleanliness <= 80
review scores checkin <= 70
review scores communication <= 70
review scores location <= 75
review scores value <= 80
-delete many lists
$or - used like an OR operator
$lte - less than or equal
db.listingsAndReviews.deleteMany(
  {
    $or:
    [ 
      {"review_scores.review_scores_accuracy": {$lte: 75}},
      {"review_scores.review_scores_cleanliness": {$lte: 80}},
      {"review_scores.review_scores_checkin": {$lte: 70}},
      {"review_scores.review_scores_communication": {$lte: 70}},
      {"review_scores.review_scores_location": {$lte: 75}},
      {"review_scores.review_scores_value": {$lte: 80}}
    ]
  }
)

$gt - greater than 
$gte - greater than or equal
*/